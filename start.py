import logging
import logging.config

logging.config.fileConfig('logging.conf')
logger = logging.getLogger(__name__)
logger.info('Starting Import Script')

import sys
from hxl2ckan.hxl_data_manager import HxlDataManager
from hxl2ckan.ckan_pusher import CkanPusher

if len(sys.argv) == 5:
    hxl_file_url = sys.argv[1]
    ckan_url = sys.argv[2]
    datastore_id = sys.argv[3]
    api_key = sys.argv[4]

    hxl_data_manager = HxlDataManager(hxl_file_url)
    hxl_data_manager.process()

    ckan_pusher = CkanPusher(ckan_url, api_key)
    if ckan_pusher.create_datastore(datastore_id, schema=hxl_data_manager.schema(), indexes=hxl_data_manager.indexes()):
        for value_list in hxl_data_manager.next_chunk():
            ckan_pusher.push_rows_to_datastore(datastore_id, value_list)
            logger.info('Chunk pushed')
else:
    logger.error("The following arguments need to be specified: hxl_file_url, ckan_url, datastore_id, api_key")

logger.info("Finished")


