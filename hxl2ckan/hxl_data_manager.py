import logging
from hxl import data, model

logger = logging.getLogger(__name__)

class HxlDataManager(object):
    '''
    Helps import HXLized data (url) to a CKAN instance.
    2 steps:
    1) Read the HXLized data - get access to metadata
    2) Import to CKAN
    '''

    rules = [
        {
            'tag': '#affected',
            'conclusion': 'int'
        },
        {
            'attributes': ['year'],
            'conclusion': 'int'
        },
        {
            'conclusion': 'text'
        }
    ]

    @staticmethod
    def _filter_col_name_chars(original_name):
        return original_name.replace('#', '').replace('+', '_').replace(' ', '')

    def __init__(self, url):
        self.chunk_size = 6000

        self._url = url

        self.hxldata = None
        # :type : hxl.io.HXLReader

        self.column_types = None

    def process(self):
        '''
        Parse the data and create the hxl object.
        Guess column types.
        '''
        self.hxldata = data(self._url)
        self._compute_metadata()

    def _compute_metadata(self):
        '''
        Guess column types
        '''
        column_types = []

        def matches(column, rule):
            '''
            :param column: HXL column model
            :type column: model.Column
            :param rule: rule
            :type rule: dict
            :return: True if the rule matches the column. Otherwise False
            :rtype: bool
            '''

            if rule.get('tag') and rule['tag'] != column.tag:
                return False

            for attribute in rule.get('attributes', []):
                if attribute not in column.attributes:
                    return False

            return True

        def compute_column_type(column):
            '''
            :param column: HXL column model
            :type column: model.Column
            :return: the datastore / postgres column type
            :rtype: str
            '''

            try:
                for rule in self.rules:
                    if matches(column, rule):
                        return rule['conclusion']
            except Exception, e:
                ex_msg = e.message if hasattr(e, 'message') else str(e)
                logger.error(
                    'Exception occured: {} while processing {}. Assuming text col type'.format(ex_msg, column))
                return 'text'

        for col in self.hxldata.columns:

            column_types.append({
                'name': col.tag,
                'display_name': col.display_tag,
                'header': col.header,
                'type': compute_column_type(col)
            })

        logger.info('Column types: {}'.format(column_types))
        self.column_types = column_types


    def schema(self):
        return [{'id': self._filter_col_name_chars(col['display_name']), 'type': col['type']} for col in
                self.column_types]

    def indexes(self):
        return [self._filter_col_name_chars(col['display_name']) for col in self.column_types]

    def next_chunk(self):
        ret_list = []
        for index, row in enumerate(self.hxldata):
            row = row
            ''':type : model.Row'''
            dict_row = {}
            for i,val in enumerate(row.values):
                type = self.column_types[i]['type']
                col_name = self._filter_col_name_chars(row.columns[i].display_tag)
                val = val if val else None
                if type == 'int' and val:
                    try:
                        int(val)
                    except ValueError, e:
                        logger.warn('At line {}: value "{}" is not {}'.format(index+1, val, type))
                        dict_row = None
                        break
                dict_row[col_name] = val

            if dict_row:
                ret_list.append(dict_row)
            if len(ret_list) == self.chunk_size:
                yield ret_list
                ret_list = []

        if len(ret_list) > 0:
            yield ret_list