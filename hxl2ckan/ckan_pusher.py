import logging
import ckanapi

logger = logging.getLogger(__name__)

class CkanPusher(object):
    def __init__(self, ckan_url, api_key):
        self.ckan = ckanapi.RemoteCKAN(ckan_url, apikey=api_key)

    def delete_datastore(self, datastore_id):
        try:
            self.ckan.action.datastore_delete(resource_id=datastore_id, force=True)
        except Exception, e:
            ex_msg = e.message if hasattr(e, 'message') else str(e)
            logger.warn(
                'Datastore could not be deleted: {}'.format(ex_msg))

    def create_datastore(self, datastore_id, schema, primary_key=None, indexes=None, delete_first=True):
        '''
        :param datastore_id: the id of the resource / datastore
        :type datastore_id: str
        :param schema:
        :type schema: list[dict]
        :param primary_key:
        :type primary_key: str
        :param indexes:
        :type indexes: list[str]
        :param delete_first:
        :type delete_first: bool
        :return: True if the datastore was created. False if a problem was encountered.
        :rtype: bool
        '''

        if delete_first:
            self.delete_datastore(datastore_id)
        try:
            self.ckan.action.datastore_create(resource_id=datastore_id, force=True, fields=schema, indexes=indexes,
                                              primary_key=primary_key)
            return True
        except Exception, e:
            ex_msg = e.message if hasattr(e, 'message') else str(e)
            logger.warn(
                'Datastore could not be created: {}'.format(ex_msg))
            return False

    def push_rows_to_datastore(self, datastore_id, records):
        self.ckan.action.datastore_upsert(
                        resource_id=datastore_id,
                        force=True,
                        method='insert',
                        records=records)